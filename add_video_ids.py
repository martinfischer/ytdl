"""
add_video_ids.py

Fügt die Video IDs eines Channels in die video_ids.txt Datei ein ohne die videos
Videos herunterzuladen. Gut, um neue Channels hinzuzfügen.
"""
import ytdl
import argparse


parser = argparse.ArgumentParser(description="Add video ids without downloading them")
parser.add_argument(
    "-i",
    "--id",
    metavar="ID",
    nargs=1,
    help="channel id",
    required=True,
)

args = parser.parse_args()

channel_id = args.id[0]
uploads_playlist_id = ytdl.get_uploads_playlist_id(channel_id)
video_ids_and_titles = ytdl.get_uploads_video_ids_and_titles(uploads_playlist_id)

video_id_list = list(video_ids_and_titles.keys())

counter = 0
video_ids = ""
for video_id in video_id_list:
    video_ids += "{}\n".format(video_id)
    counter += 1

ytdl.append_to_video_ids(video_ids)

print(counter, "video_ids added to video_ids.txt without downloading the videos")
