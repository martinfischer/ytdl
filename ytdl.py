"""
ytdl.py

Lädt mit Hilfe von youtube-dl neue Videos aus Youtube Channels herunter
"""
import os
import requests
import settings
from time import sleep
from subprocess import call, getoutput
from termcolor import colored
from datetime import datetime


API_KEY = settings.API_KEY
API_URL_CHANNELS = "https://www.googleapis.com/youtube/v3/channels"
API_URL_PLAYLISTITEMS = "https://www.googleapis.com/youtube/v3/playlistItems"
YOUTUBE_VIDEO_URL_BASE = "http://www.youtube.com/watch?v="
JUST_DO_IT_FILE = "channel_ids.txt"
ASK_FIRST_FILE = "channel_ids_ask_first.txt"
VIDEO_IDS_TXT = os.path.expanduser("~/Dropbox/video_ids.txt")
MAXRESULTS = "50"


def get_uploads_playlist_id(channel_id):
    """
    Holt die Playlist ID für die Uploads aus dem jeweiligen Channel
    """
    payload = {
        "part": "contentDetails",
        "id": channel_id,
        "fields": "items/contentDetails/relatedPlaylists/uploads",
        "key": API_KEY
    }
    r = requests.get(API_URL_CHANNELS, params=payload)
    if r.status_code != 200:
        return "Error with status code: " + str(r.status_code)
    item = r.json()["items"][0]
    uploads_playlist_id = item["contentDetails"]["relatedPlaylists"]["uploads"]
    return uploads_playlist_id


def get_uploads_video_ids_and_titles(uploads_playlist_id):
    """
    Holt die Video IDs aus der jeweiligen Upload Playlist
    """
    payload = {
        "part": "snippet",
        "maxResults": MAXRESULTS,
        "playlistId": uploads_playlist_id,
        "fields": "items/snippet",
        "key": API_KEY
    }
    r = requests.get(API_URL_PLAYLISTITEMS, params=payload)
    if r.status_code != 200:
        return "Error with status code: " + str(r.status_code)
    uploads_video_ids = {}
    for item in r.json()["items"]:
        video_id = item["snippet"]["resourceId"]["videoId"]
        title = item["snippet"]["title"]
        uploads_video_ids[video_id] = title
    return uploads_video_ids


def read_channel_ids_and_names(filename):
    """
    Liest die Channel IDs und die Channel Namen aus einer Datei
    """
    with open(filename) as text_file:
        channel_ids = {}
        for line in text_file:
            channel_id = line.split(":::")[0].strip()
            channel_name = line.split(":::")[1].strip()
            channel_ids[channel_id] = channel_name
        return channel_ids


def channel_ids(filename):
    """
    Holt die Channel IDs
    """
    return list(read_channel_ids_and_names(filename).keys())


def channel_name(filename, channel_id):
    """
    Holt die Channel Namen
    """
    return read_channel_ids_and_names(filename)[channel_id]


def read_video_ids():
    """
    Liest vorhandene Video IDs aus einer Datei
    """
    with open(VIDEO_IDS_TXT) as text_file:
        video_ids = []
        for line in text_file:
            video_ids.append(line.strip())
        return video_ids


def append_to_video_ids(video_ids):
    if video_ids:
        number = len(video_ids.split())
        print(colored("Writing {} IDs to {}".format(number, VIDEO_IDS_TXT), "grey", attrs=["bold"]))
        with open(VIDEO_IDS_TXT, "a") as text_file:
            text_file.write(video_ids)
    else:
        print(colored("Not writing {}, no IDs".format(VIDEO_IDS_TXT), "grey", attrs=["bold"]))


def video_allready_downloaded(video_id):
    if video_id in read_video_ids():
        return True
    return False


def download(video_id):
    """
    Lädt ein Video herunter mit Hilfe von youtube-dl
    """
    try:
        working_dir = os.getcwd()
        os.chdir(os.path.expanduser("~/Videos/TV/C Tube/"))

        title_command = ("youtube-dl -f best --get-filename -o '%(title)s' ") + YOUTUBE_VIDEO_URL_BASE + video_id
        title = getoutput(title_command).strip()
        title = title.replace("%", "_")
        command = ("youtube-dl -f best --console-title -o \"%(uploader)s - %(upload_date)s - {:.50} - %(id)s.%(ext)s\" ".format(title)) + YOUTUBE_VIDEO_URL_BASE + video_id
        call(command, shell=True)
        os.chdir(working_dir)
        return True
    except Exception as error:
        print(error)
        return False


def just_do_it(filename):
    """
    Startet Downloads ohne nachzufragen
    """
    just_do_it_channels = channel_ids(filename)
    video_ids = ""
    for channel_id in just_do_it_channels:
        channel = channel_name(filename, channel_id)
        print("Looking for new videos from {channel}".format(channel=channel))
        uploads_playlist_id = get_uploads_playlist_id(channel_id)
        sleep(0.2)
        video_ids_and_titles = get_uploads_video_ids_and_titles(uploads_playlist_id).items()
        sleep(0.2)
        for video_id, title in video_ids_and_titles:
            if video_allready_downloaded(video_id):
                pass
            else:
                video_ids += "{}\n".format(video_id)
                ytext = colored(
                    "Downloading {title} {youtube_video_url} from {channel}".format(
                        title=title,
                        youtube_video_url=YOUTUBE_VIDEO_URL_BASE + video_id,
                        channel=channel
                    ), "green", attrs=["bold"]
                )
                print(ytext)
                download(video_id)
    return video_ids


def ask_first(filename):
    """
    Startet Downloads mit Nachfrage
    """
    ask_first_channels = channel_ids(filename)
    video_ids = ""
    for channel_id in ask_first_channels:
        channel = channel_name(filename, channel_id)
        print("Looking for new videos from {channel}".format(channel=channel))
        uploads_playlist_id = get_uploads_playlist_id(channel_id)
        sleep(0.2)
        video_ids_and_titles = get_uploads_video_ids_and_titles(uploads_playlist_id).items()
        sleep(0.2)
        for video_id, title in video_ids_and_titles:
            if video_allready_downloaded(video_id):
                pass
            else:
                video_ids += "{}\n".format(video_id)
                dtext = colored(
                    "Download {title} {youtube_video_url} from {channel}? Type y or yes to download: ".format(
                        title=title,
                        youtube_video_url=YOUTUBE_VIDEO_URL_BASE + video_id,
                        channel=channel
                    ), "blue", attrs=["bold"]
                )
                print(dtext)
                answer = input().strip()
                if answer == "y" or answer == "yes":
                    ytext = colored(
                        "Downloading {title} {youtube_video_url} from {channel}".format(
                            title=title,
                            youtube_video_url=YOUTUBE_VIDEO_URL_BASE + video_id,
                            channel=channel), "green", attrs=["bold"]
                    )
                    print(ytext)
                    download(video_id)
                else:
                    ntext = colored("Not downloading", "yellow", attrs=["bold"])
                    print(ntext)
    return video_ids


if __name__ == "__main__":
    start_time = datetime.now()

    video_ids = just_do_it(JUST_DO_IT_FILE)

    # Parst Kommandozeilen Argumente, falls ein "y" oder "yes" vorhanden ist,
    # werden Downloads ohne nachzufragen gestartet
    import argparse
    parser = argparse.ArgumentParser(description="Download all the things")
    parser.add_argument(
        "-y",
        "--yes",
        action="store_true",
        help="download all the things",
        required=False,
    )
    args = parser.parse_args()
    if args.yes:
        video_ids += just_do_it(ASK_FIRST_FILE)
    else:
        video_ids += ask_first(ASK_FIRST_FILE)

    append_to_video_ids(video_ids)

    end_time = datetime.now()
    delta = end_time - start_time

    ftext = colored("*** ALL DONE --- THIS TOOK {} SECONDS ***".format(delta.seconds), "green", attrs=["bold"])
    print(ftext)
